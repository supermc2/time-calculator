import {Component, OnInit, ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
import * as airports from '../assets/airports.json';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  title = 'timezone-calculator';
  options: any[];
  optionsBuffer = [];
  bufferSize = 50;
  filteredOptions: Observable<any[]>;
  departureDateControl = new FormControl();
  airportControl = new FormControl();
  daysControl = new FormControl();
  hongkongTime: String;
  hongkongAddTime: String;
  DT_FORMAT = 'YYYY-MM-DD HH:mm:ss';
  departureDateModel: String;
  departureTimeModel: String;
  dateExample: any;
  daysModel: Number;
  departureAirportModel: any;
  enteredDate: Date;
  private _isPickerOpen = false;
  hideCalendar = true;

  @ViewChild('picker', {static: false}) picker: any;
  numberOfItemsFromEndBeforeFetchingMore = 10;
  loading = false;

  constructor() {
    console.log(airports['default']);
   }

   ngOnInit() {
     this.daysControl.setValue(14);
     //this.departureDateControl.setValue(moment().format('YYYY-MM-DD HH:mm:ss'));
     this.options = airports['default'];
  }

  private toHkt(dateTime, timeZone) {
    let from = moment.tz(dateTime, this.DT_FORMAT, timeZone);
      // https://momentjs.com/timezone/docs/#/using-timezones/parsing-in-zone/
    const dateTimeString = from.format('YYYY-MM-DD HH:mm:ss');
    from = moment.tz(''+ dateTimeString +'', this.DT_FORMAT, timeZone);
    // https://momentjs.com/timezone/docs/#/using-timezones/parsing-in-zone/
    return from.tz('Asia/Hong_Kong').format(this.DT_FORMAT);
  }

  updateValue() {
    this.hongkongAddTime = "";
    if(this.departureAirportModel== null || this.departureDateControl.value == null){
    
      return;
    }
    let fromDateString = moment(this.departureDateControl.value).format(this.DT_FORMAT);
    this.hongkongTime = this.toHkt(fromDateString, this.departureAirportModel.time_zone);
    this.hongkongAddTime = moment(this.hongkongTime, this.DT_FORMAT).add(336, 'hours').format(this.DT_FORMAT);
    console.log(this.hongkongTime);
    console.log(this.hongkongAddTime);
  }


  private fetchMore() {
    const len = this.optionsBuffer.length;
    const more = this.options.slice(len, this.bufferSize + len);
    this.loading = true;
    // using timeout here to simulate backend API delay
    setTimeout(() => {
        this.loading = false;
        this.optionsBuffer = this.optionsBuffer.concat(more);
    }, 200);
}

onScrollToEnd() {
  this.fetchMore();
}

onScroll({ end }) {
  if (this.loading || this.options.length <= this.optionsBuffer.length) {
      return;
  }

  if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.optionsBuffer.length) {
      this.fetchMore();
  }
}

customSearchFn(term: string, item: any) {
  term = term.toLowerCase();
  //return item.name.toLowerCase().indexOf(term) > -1 || item.gender.toLowerCase() === term;
  return item.airport_name.toLowerCase().includes(term) ||
      item.city.toLowerCase().includes(term);
}
toggleCalendar(){
  this.hideCalendar = !this.hideCalendar;
}
updateDepartureDate(){
  console.log('hihi');
}
}
